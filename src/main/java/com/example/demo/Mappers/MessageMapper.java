package com.example.demo.Mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface MessageMapper {

    @Select("SELECT TEXT FROM MESSAGE WHERE ID=#{ID}")
    String findById(@Param("ID") int id);

}
