package com.example.demo;

import com.example.demo.Mappers.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    MessageMapper messageMapper;

    public String getTextById(int id) {
        return messageMapper.findById(id);
    }
}
