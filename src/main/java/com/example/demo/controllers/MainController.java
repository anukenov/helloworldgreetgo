package com.example.demo.controllers;

import com.example.demo.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    MessageService messageService;

    @RequestMapping("/")
    public String home() {
        return "home";
    }
}
