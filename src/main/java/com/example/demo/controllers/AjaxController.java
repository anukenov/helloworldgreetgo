package com.example.demo.controllers;

import com.example.demo.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AjaxController {

    @Autowired
    MessageService messageService;

    @RequestMapping(value = "/ajax", method = RequestMethod.GET)
    public @ResponseBody
    String home() {
        String message_text = messageService.getTextById(1);
        String res = "<h1>" + message_text + "</h1>";
        return res;
    }
}
