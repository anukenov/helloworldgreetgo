HelloWorldGreetgo

1.1. Чтобы скачать проект запускаем команду: $ git clone https://gitlab.com/Nukenov/helloworldgreetgo.git

2.1. Заходим в папку: "helloworldgreetgo"

3.1. Запускаем команду: $ gradle bootRun

4.1. Ждём когда запуститься сервер. После запуска сервера заходим в браузере по адресу: 
http://locahost:8080/

5.1. И видим результат вывода текста "Hello World!"

6.1. Заходим по адресу: http://locahost:8080/h2-console/
6.2. Поля заполнить согласно нижеперечисленному:
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:mem:testdb
User Name: sa
Password: оставить пустым
6.3. В верхнем левом углу нажать на таблицу "MESSAGE" и нажать кнопку Run
6.4. Нажать кнопку edit и меняем сообщение "Hello World!" на любую другую.
6.5.1 Если кнопки edit нет, в текстовом поле пишем sql команду: 
UPDATE MESSAGE
SET TEXT = '(заменить сообщение на любую другую)' 
WHERE ID = 1; 
6.5.2 И нажимаем на кнопу Run

7.1. Переходим по адресу: http://locahost:8080/ и смотрим что сообщение обновилось.